Variables de decision:\
X1: Numero de pantalones que se confeccionan diariamente\
X2: Numero de blusas que se confeccionan diariamente

Restricciones:\
3X1 + X2 <= 3000\
4X1 + 3X2 <= 6000\
X1 >= 400

Funcion objetivo:\
Maximizar:\
Z = 4000X1 + 3000X2

Intersecciones:\
L2 y L3:\
X1 == 400\
4X1 + 3X2 == 6000\
1600 + 3X2 == 6000\
3X2 = 4400\
X2 = 4400/3 = 1466.66 -> 1466

L1 Y L2:\
9X1 + 3X2 == 9000\
4X1 + 3X2 == 6000\
5X1 = 3000\
X1 = 600\
X2 == 1200


F(400,0) =                           1600000\
F(1000,0) =                          4000000\
F(600,1200) = 600x4000 + 1200x3000 = 6000000 <- SOLUCION OPTIMA\
F(400,1466) = 400x4000 + 1466x3000 = 5998000

Respuesta:\
600 pantalones\
1200 blusas


