Variables de decision:
X1: Numero de horas a la semana que trabaja juan en la tienda 1
X2: Numero de horas a la semana que trabaja juan en la tienda 2

Restriccion:
X1 + X2 >= 20
5 <= X1 <=12
6 <= X2 <=10

Funcion objetivo: (MINIMIZAR)
Z = 8X1 + 6X2

Rango de existencia:
X1, X2 >= 0

![](2020-04-16-23-18-29.png)

F(12,8) = 96 + 48 = 144
F(10,10) = 140 <- SOLUCION OPTIMA
F(12,10) = 96 + 60 = 156

respuesta:
Deberia trabajar 10 hrs en la tienda 1
y 10 horas en la tienda 2