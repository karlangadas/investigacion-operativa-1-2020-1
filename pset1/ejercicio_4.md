```
a)
Variables de decision:
X1: N de ensaladeras a fabricar
X2: N de tazones a fabricar
P: N placas

Placa:
X1 + 2X2 o 6x2

Funcion objetivo: (maximizar)
Z = 80X1 + 25X2 -60P

Restricciones:
4X1<=X2
P<=680

b)
Variables de decision:
X1: N de ensaladeras a fabricar
X2: N de tazones a fabricar
P: N placas

Placa:
1P = X1 + 2X2 o 6x2

P(X) => Cantidad de P que se necesita para hacer X

Sabemos que:
P(tazon) = 1/6

Entonces:
1P(ensaladera) + 2P(tazon) = 1
P(ensaladera) = 2/3

Luego, el costo asociado a las placas:
(X1*P(ensaladera)+X2*P(tazon))*60
(X1*2/3+X2*1/6)*60
costo: 40X1+10X2

Funcion objetivo: (maximizar)
Z = 80X1 + 25X2 - (40X1+10X2)
Z = 40X1 + 15X2

Restricciones:
4X1<=X2
```
Puntos que definen la recta:

Una placa:

|X1|X2| P|
|--|--|--|
| 1| 2| 1|
| 0| 6| 1|

680 placas:

| X1|  X2|  P|
|---|----|---|
|680|1360|680|
|  0|4080|680|

```
Puntos
(0,4080) (680,1360)
Recta:
X2 = (1360-4080)/680X1 + 4080
X2 = -4X1 + 4080
4X1 + X2 <=4080

Restricciones:
4X1<=X2
4X1 + X2 <=4080

Rango de existencia:
X1,X2>=0

Intersecciones:
L1 Y L2:
4X1=X2
X1 + X2 = 4080
X1 + 4X1 = 4080
5X1 = 4080
X1 = 816
X2 = 3264
```


```
Puntos:
F(0,4080) = 61200
F(816,3264) = 81600 <-SOLUCION OPTIMA
Respuesta:
816 ensaladas
3264 tazones
```


