```
Variables de decision:
X1: Numero de bebidas de marca A
X2: Numero de bebidas de marca B

Funcion objetivo:
Z = 5X1 + 7X2

Restricciones:
X1 + X2 <= 500
2X1 <= X2
X1 >= 100


X1 = 500/3 = 166.6 -> 166
500 - 166 = 334
X2 = 1000/3 = 333.3 -> 332


X1 + X2 == 500
X1 == 100
```
![](2020-04-17-00-22-28.png)
```
F(100,400) = 500 + 2800 = 3300 <- SOLUCION OPTIMA
F(166,334) = 830 + 2338 = 3168

respuesta:
100 bebidas marca A
400 bebidas marca B
```